;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'transient-mount
  authors "Dima Akater"
  first-publication-year-as-string "2020"
  org-files-in-order '("transient-mount-core"
                       "transient-mount")
  test-phase-enabled nil
  site-lisp-config-prefix "50"
  license "GPL-3")
